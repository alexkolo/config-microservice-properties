package com.rest.resttesting.repositories;

import com.rest.resttesting.model.Customer;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;


/**
 * 
 * Los find by .... dependen del pojo, con ponerlo no hace falta implementacion
 * 
 * @author Alejandro.Colodrero
 *
 */
public interface MongoRepositoryInterface extends MongoRepository<Customer, String> {

    public List<Customer> findByFirstName(String firstName);
    public List<Customer> findByLastName(String lastName);

}
