package com.rest.resttesting.controller;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 
 * To override message returned
 * 
 * @author Alejandro.Colodrero
 *
 */
@ControllerAdvice
class ConsumerNotFoundAdvice {

	@ResponseBody
	@ExceptionHandler(ConsumerNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	
	String employeeNotFoundHandler(ConsumerNotFoundException ex) {
		return ex.getMessage();
	}
	
}