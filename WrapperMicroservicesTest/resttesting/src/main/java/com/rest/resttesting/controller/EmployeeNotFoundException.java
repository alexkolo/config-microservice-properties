package com.rest.resttesting.controller;

class ConsumerNotFoundException extends RuntimeException {

	ConsumerNotFoundException(String id) {
		super("Could not find employee " + id);
	}
	
}