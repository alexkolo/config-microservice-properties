package com.rest.resttesting.controller;
import com.rest.resttesting.model.Customer;
import com.rest.resttesting.repositories.MongoRepositoryInterface;
	
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestMongoController {

	@Autowired
	private MongoRepositoryInterface repository;
	
//	@Autowired
//	private CustomerRepository customerRepository;

	@PostMapping("/insert")
	public Customer insertUser(@RequestBody Customer newEmployee) {
		return repository.save(newEmployee);
	}

	@RequestMapping("/getbyname")
	public List<Customer> getByName(@RequestParam(value="name", required=true) String name) {
		return repository.findByFirstName(name);
	}
	
	@RequestMapping("/getbylastname")
	public List<Customer> getByLastName(@RequestParam(value="last-name", required=true) String name) {
		
		return repository.findByLastName(name);

	}

	@RequestMapping("/getbyid")
	public Customer getById(@RequestParam(value="id", required=true) String id) {

		return repository.findById(id)
				.orElseThrow(() -> new ConsumerNotFoundException(id));

	}

	@RequestMapping("/getall")
	public List<Customer> getAll() {

		List<Customer> findAll = repository.findAll();

		return findAll;

	}

}