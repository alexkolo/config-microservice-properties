package com.rest.resttesting;

import com.rest.resttesting.model.Customer;
import com.rest.resttesting.model.Quote;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ResttestingApplication {

	private static final Logger log = LoggerFactory.getLogger(ResttestingApplication.class);

	
	public static void main(String[] args) {
		SpringApplication.run(ResttestingApplication.class, args);
        RestTemplate restTemplate = new RestTemplate();

        Quote quote = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/api/random", Quote.class);
        log.info(quote.toString());

        
        // When Unique Object
        Customer[] customers2 = restTemplate.getForObject("http://localhost:8080/getbyname?name=Bob", Customer[].class ); 
        
        log.info("With params::" + customers2.toString());
        
		System.out.println("---------------- CONSUME TESTING X 10000 ------------");
		long time = new Date().getTime();
        
        // When Multiple Object
        Customer[] customers = restTemplate.getForObject("http://localhost:8080/getbyname?name=Alice", Customer[].class ); 
   
//        log.info("With params, and array::" + customers.toString());
		long time2 = new Date().getTime();
		
		System.out.println("Diference is:" + (time2 - time));
        
	}

}
