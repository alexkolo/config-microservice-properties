package com.tester.rest.caller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.web.client.RestTemplate;

public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void restCall() {
		
		
		RestTemplate restTemplate = new RestTemplate();
		
		restTemplate.getInterceptors().add(
				  new BasicAuthenticationInterceptor("admin", "password"));
		
		Employee employee = 
				restTemplate.getForObject(
						"http://localhost:9100/employee-dashboard/dashboard/feign/1", 
						Employee.class);
		

		String string = employee.toString();
		
		System.out.println("String is:" + string);
		
	}
	
	public static void main(String[] args) {
		Application.restCall();
	}
	
	
}