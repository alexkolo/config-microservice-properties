package com.example.EmployeeSearchService;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.EmployeeSearchService.domain.model.Employee;
import com.example.EmployeeSearchService.service.EmployeeSearchService;
@RefreshScope
@RestController
public class EmployeeSearchController {
	@Autowired
	EmployeeSearchService employeeSearchService;
	@RequestMapping("/employee/find/{id}")
	public Employee findById(@PathVariable Long id) {
		return employeeSearchService.findById(id);
	}
	@RequestMapping("/employee/findall")
	public Collection < Employee > findAll() {
		
		System.out.println("Server 101  returning all");
		
		return employeeSearchService.findAll();
	}
	
	//CONFIG TEST POURPOUSES
	// message = Hello world
    @Value("${message:Hello default when NO message}")
    private String message;
    
    @RequestMapping("/config/message")
    public String getMessage() {
        return this.message;
    }
	
}