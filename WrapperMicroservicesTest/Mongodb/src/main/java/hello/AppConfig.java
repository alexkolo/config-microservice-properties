package hello;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    public String foo() {
    	String foo = "hellou";
    	bar();
        return foo;
    }

    @Bean
    public String bar() {
        return "bar";
    }

    @Bean
    public String blah() {
        return "blah";
    }
}