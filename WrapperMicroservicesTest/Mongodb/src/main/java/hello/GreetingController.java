package hello;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
	
	@Autowired
	private CustomerRepository repository;

	//http://localhost:8080/hello?name=1
    @RequestMapping("/hello")
    public Customer greeting(@RequestParam(value="name", required=true) int id) {

    	List<Customer> findAll = repository.findAll();

    	return findAll.get(id);
    }
}