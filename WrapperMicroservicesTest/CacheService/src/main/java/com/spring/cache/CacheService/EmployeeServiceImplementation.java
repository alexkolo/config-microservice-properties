package com.spring.cache.CacheService;

import com.spring.cache.Pojos.Employee;
import com.spring.cache.Pojos.EmployeeRepository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@CacheConfig(cacheNames={"employees"})
@Component
public class EmployeeServiceImplementation implements EmployeeRepository {

	// This is OUR SERVICE IN THIS CASE :)
	private static Map < Long, Employee > employeeRepsitory = new HashMap<>();
	
    @Override
    @Cacheable(value = "mycache", key="#employeeId")
	public Employee getById(Long employeeId) {
    	return employeeRepsitory.get(employeeId);
	}
    
    @Override
    @CachePut(value = "mycache", key="#employeeId")
	public Employee updateById(Long employeeId) {
    	return employeeRepsitory.get(employeeId);
	}

	@Override
	public Employee createEmployee(Employee employee) {
		employee.setEmployeeId(Long.valueOf(employeeRepsitory.size()));
		return employeeRepsitory.put(Long.valueOf(employeeRepsitory.size()), employee);
	}

    @Override
    @Cacheable
	public Collection<Employee> getAllEmployees() {
    	System.out.println("Getting all the values");
		return employeeRepsitory.values();
	}

	@Override
	@CachePut
	public Boolean deleteEmployee(Long employeeId) {
		
		Boolean toReturn = false;
		
		if(employeeRepsitory.get(employeeId) != null) {
			employeeRepsitory.remove(employeeId);
			toReturn = true;
		}
		
		return toReturn;
		
	}

	
	private static Employee createEmployee(Long id, String name, String practiceArea, String designation) {
		Employee emp = new Employee();
		emp.setEmployeeId(id);
		emp.setName(name);
		emp.setPracticeArea(practiceArea);
		emp.setDesignation(designation);
		emp.setCompanyInfo("Cognizant");
		return emp;
	}


	
	
//	static {
		
//		Stream < String > employeeStream = Stream.of("1,Shamik  Mitra,Java,Architect", "2,Samir  Mitra,C++,Manager",
//				"3,Swastika  Mitra,AI,Sr.Architect");

//		employeeStream.stream().filter(s -> s.length() == 2).forEach(System.out::println);
//		employeeRepsitory.put(key, value)
//		
//		EmployeeRepsitory = employeeStream.map(employeeStr -> {
//			String[] info = employeeStr.split(",");
//			return createEmployee(new Long(info[0]), info[1], info[2], info[3]);
//		}).collect(Collectors.toMap(Employee::getEmployeeId, emp -> emp));
		
//	}
	
    // Don't do this at home :) 
//  private void simulateSlowService() {
//      try {
//          long time = 3000L;
//          Thread.sleep(time);
//      } catch (InterruptedException e) {
//          throw new IllegalStateException(e);
//      }
//  }

}
