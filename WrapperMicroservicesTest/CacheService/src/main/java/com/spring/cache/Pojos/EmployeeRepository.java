package com.spring.cache.Pojos;

import java.util.Collection;

public interface EmployeeRepository {

	Employee getById(Long employeeId);
	Employee createEmployee (Employee employee);
	Collection <Employee> getAllEmployees();
	Boolean deleteEmployee (Long employeeId);
	Employee updateById(Long employeeId);

}