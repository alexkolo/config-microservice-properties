package com.spring.cache.Controller;
import com.spring.cache.CacheService.EmployeeServiceImplementation;
import com.spring.cache.Pojos.Employee;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class EmployeeSearchController {
	
	private static final Logger logger = 
			LoggerFactory.getLogger(EmployeeSearchController.class);
	
	@Autowired
	EmployeeServiceImplementation simpleEmployeeRepository;
	
	@RequestMapping("/employee-service/create")
	public Employee create() {		
		 Employee createdEmployee = simpleEmployeeRepository.createEmployee(
				new Employee("Created User", "Java", "Architect", "Omega's" ));	
		 
		 return createdEmployee;
	}
	
	@RequestMapping("/employee-service/find/{employeeId}")
	public Employee findById(@PathVariable Long employeeId) {
		return simpleEmployeeRepository.getById(employeeId);
	}

	@RequestMapping("/employee-service/findall")
	public Collection < Employee > findAll() {
		return simpleEmployeeRepository.getAllEmployees();
	}
	
	@RequestMapping("/employee-service/delete/{employeeId}")
	public Boolean deleteById(@PathVariable Long employeeId) {		
		return simpleEmployeeRepository.deleteEmployee(employeeId);
	}

}