package com.omega.Graphql.repository;

import com.omega.Graphql.model.Author;

import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> { }