package com.omega.Graphql.repository;

import com.omega.Graphql.model.Book;

import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> { }