package com.omega.Graphql.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.omega.Graphql.model.Author;
import com.omega.Graphql.model.Book;
import com.omega.Graphql.repository.AuthorRepository;

public class BookResolver implements GraphQLResolver<Book> {
    private AuthorRepository authorRepository;

    public BookResolver(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public Author getAuthor(Book book) {  	
        return authorRepository.findById(book.getAuthor().getId()).get();
    }
}