package com.omega.Graphql.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.omega.Graphql.model.Author;
import com.omega.Graphql.model.Book;
import com.omega.Graphql.repository.AuthorRepository;
import com.omega.Graphql.repository.BookRepository;

public class Query implements GraphQLQueryResolver {
    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    public Query(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    public Iterable<Book> findAllBooks() {
        return bookRepository.findAll();
    }

    public Iterable<Author> findAllAuthors() {
        return authorRepository.findAll();
    }

    public long countBooks() {
        return bookRepository.count();
    }
    public long countAuthors() {
        return authorRepository.count();
    }
}