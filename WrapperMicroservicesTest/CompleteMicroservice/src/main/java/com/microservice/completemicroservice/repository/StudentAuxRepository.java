package com.microservice.completemicroservice.repository;

import com.microservice.completemicroservice.entities.Student;

import java.util.Collection;
import java.util.Optional;

/**
 * 
 * To made intermediate into service and caller
 * 
 * @author Alejandro.Colodrero
 *
 */
public interface StudentAuxRepository {

	String getType();
	Optional<Student> getById(Long studentId);
	Student createStudent (Student student);
	Collection <Student> getAllStudents();
	Boolean deleteStudents (Long studentId);
	Student updateById(Student student);
	
}
