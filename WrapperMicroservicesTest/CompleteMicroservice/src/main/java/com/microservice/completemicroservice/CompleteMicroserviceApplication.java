package com.microservice.completemicroservice;

import com.microservice.completemicroservice.repository.StudentRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import java.util.logging.Logger;

//import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableCaching
@EnableDiscoveryClient
public class CompleteMicroserviceApplication implements CommandLineRunner {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	StudentRepository repository;
	
	public static void main(String[] args) {
		SpringApplication.run(CompleteMicroserviceApplication.class, args);
	}
	
	// AUTOLAUNCH WHEN INIT APPLICATION -- UNUSED
	@Override
	public void run(String... args) throws Exception {

//		logger.info("Student id 10001 -> {}", repository.findById(10001L));
//
//		logger.info("Inserting -> {}", repository.save(new Student(10004L, "the 4", "A1234657")));
//
//		logger.info("Update 10003 -> {}", repository.save(new Student(10003L, "the 3", "New-Passport")));

//		repository.deleteById(10002L);

//		logger.info("All users -> {}", repository.findAll());
	}

}
