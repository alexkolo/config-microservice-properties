package com.microservice.completemicroservice.service;

import com.microservice.completemicroservice.repository.StudentAuxRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceFactory {

    @Autowired
    private List<StudentAuxRepository> services;

    private static final Map<String, StudentAuxRepository> myServiceCache = new HashMap<>();

    @PostConstruct
    public void initMyServiceCache() {
    	
        for(StudentAuxRepository service : services) {      	
            myServiceCache.put(service.getType(), service);           
        }
        
    }

    public StudentAuxRepository getService(String type) {
    	
    	StudentAuxRepository service = myServiceCache.get(type);
    	
        if(service == null) {
        	throw new RuntimeException("Unknown service type: " + type);
        }
        
        return service;
        
    }

}
