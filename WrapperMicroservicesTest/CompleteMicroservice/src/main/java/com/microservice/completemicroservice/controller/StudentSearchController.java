package com.microservice.completemicroservice.controller;
import com.microservice.completemicroservice.constants.Constants;
import com.microservice.completemicroservice.entities.Student;
import com.microservice.completemicroservice.repository.StudentAuxRepository;
import com.microservice.completemicroservice.service.ServiceFactory;

import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class StudentSearchController {

	private static final Logger logger = 
			LoggerFactory.getLogger(StudentSearchController.class);

	@Autowired
	ServiceFactory serviceFactory;

	
	@RequestMapping("/getall")
	public Collection<Student> getall() {			
		return studentServiceImpl().getAllStudents();
	}
	
	@RequestMapping("/create")
	public Student studendCreate() {			
		Student studentCreated = studentServiceImpl().createStudent(
						new Student( "the created", "PassportNumber"));				
		return studentCreated;
	}
	
	@RequestMapping("/getbyid/{studendid}")
	public Optional<Student> getById(@PathVariable Long studendid) {		
		return studentServiceImpl().getById(studendid);
	}
	
	@RequestMapping("/deletebyid/{studendid}")
	public Boolean deleteById(@PathVariable Long studentid) {		
		return studentServiceImpl().deleteStudents(studentid);
	}

	public StudentAuxRepository studentServiceImpl() {
		return serviceFactory.getService(Constants.STUDENT_SERVICE_TYPE);
	}

	public StudentAuxRepository studentServiceAltImpl() {
		return serviceFactory.getService(Constants.STUDENT_SERVICE_ALTERNATIVE_TYPE);
	}

}