package com.microservice.completemicroservice.constants;

public class Constants {
	
	public static final String STUDENT_SERVICE_ALTERNATIVE_TYPE =  "StudentServiceAlternative";
	public static final String STUDENT_SERVICE_TYPE =  "StudentService";

}
