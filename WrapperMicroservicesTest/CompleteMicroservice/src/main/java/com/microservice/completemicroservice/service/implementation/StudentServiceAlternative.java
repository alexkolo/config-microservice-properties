package com.microservice.completemicroservice.service.implementation;

import com.microservice.completemicroservice.constants.Constants;
import com.microservice.completemicroservice.entities.Student;
import com.microservice.completemicroservice.repository.StudentAuxRepository;
import com.microservice.completemicroservice.repository.StudentRepository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

public class StudentServiceAlternative implements StudentAuxRepository{

	@Autowired
	StudentRepository repository;

	@Override
	public String getType() {
		return Constants.STUDENT_SERVICE_ALTERNATIVE_TYPE;
	}
	
	@Override
	public Optional<Student> getById(Long studentId) {	
		return repository.findById(studentId);
	}

	@Override
	public Student createStudent(Student student) {
		return repository.save(student);
	}

	@Override
	public Collection<Student> getAllStudents() {
		return repository.findAll();
	}

	@Override
	public Boolean deleteStudents(Long studentId) {
		repository.deleteById(studentId);
		return true;
	}

	@Override
	public Student updateById(Student student) {
		return repository.save(student);
	}

}
